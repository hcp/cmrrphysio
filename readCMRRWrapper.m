function readCMRRWrapper(varargin)
% The readCMRRWrapper.m function takes as input 1) physio DICOM and 2) (optional) number 
% of volumes that were skipped / dropped from the beginning of the BOLD timeseries, and it
% outputs a csv file with TRIG,PULS,RESP,ACQ,EXT columns. Importantly, the TRIG column 
% marks the onset of each BOLD volume aligned with beginning of the trimmed BOLD 
% timeseries. This wrapper script calls the readCMRRPhysio.m script written by Eddie 
% Auerbach at CMRR to extract the physio vectors from the custom field in DICOM header.


%% check input arguments
% We expect up to 2 inputs 
% Input 1 = filename (physio dicom)
% Input 2 = # of volumes that are dropped from beginning of run during
% pre-processing
% 
if(nargin > 2)
    error('Invalid number of inputs.');
elseif(nargin == 2)
    fn = varargin{1};
    VolSkip = str2double(varargin{2});
elseif(nargin == 1)
    fn = varargin{1};
    VolSkip = 10;
end

fprintf('Argument information and fn/VolSkip value assignments:\n')
fprintf('nargin: %i\n',nargin)
fprintf('varargin: %s\n',varargin{:})
fprintf('fn: %s\n',fn)
fprintf('VolSkip: %i\n',VolSkip)

[filepath,name,ext]=fileparts(fn);

% 'deployed' is compiled matlab
% if not compiled matlab, change to 'filepath' directory
if(~isdeployed)
      cd(filepath);
end

%% body of reading in CMRRPhysio results

% the try/catch allows people to run dicoms as a list, and will print error
% before moving to the next dicom in the list.
try 
	% readCMRRphysio.m has 23 different possible error messages; these will be checked
	% with try / catch
    physio=readCMRRPhysio(strcat(name,ext)); 
    
    % Make vectors of zeroes just in case vectors are empty in physio struct
    % When there is no ACQ vector, there are no other vectors as well
    % When there is an ACQ vector, all other vectors match in length
    [TRIG,PULS,RESP,ACQ,EXT]=deal(zeros(length(physio.ACQ),1,'uint16'));

    % EXT channel is prioritized as EXT then EXT2. Only one of these
    % channels will have a signal. If none, then the final EXT will be blank.
    if (isfield(physio,'EXT'))
        EXT=physio.EXT;
    elseif (isfield(physio,'EXT2'))
        EXT=physio.EXT2;
    end
    if (isfield(physio,'PULS'))
        PULS=physio.PULS;
    end
    if (isfield(physio,'RESP'))
        RESP=physio.RESP;
    end
    if (isfield(physio,'ACQ'))
        ACQ=physio.ACQ;
    end

    % Compute frequency step (i.e. TR(ms) * Sample Rate)
    % For HCP-A and HCP-D this should be 0.8s * 400Hz = 320
    % Slicemap is 3D matrix, where the difference between any two columns
    % is the frequency step aka sampling rate
    FreqStep = single(diff(physio.SliceMap(1,[1 2],1)));

    % Check if we have values in the extracted EXT. If there are values,
    % see if the beginning is 0s or 1s. 
    if(sum(EXT)) 
        E = find(EXT);
        % If the first two values are '1', this means the channel is 
        % inverted (0==on,1==off), so invert it back (0==off,1==on)
        if(E(2) == 2)
            EXT = ~EXT;
        end
    end

    % Observation of ~2000 physio files indicates that the first nnz ACQ should
    % be at index ~4263. Occasionally the first nnz ACQ index is 1, in which case the 
    % second index is ~4264. So to find the 1st BOLD volume, we will check 
    % that the second index is between 4253-4273. If not, exit with error that 
    % the ACQ doesn't align with expectations.
    A = find(ACQ);
    if(A(2) >= 4253 && A(2) <= 4273)
        FirstTrig = 4256;
    else   
        error('ACQ from 1st BOLD volume is outside tolerance range. In this case, A(2)=%s: should be 4253<=A(2)<=4273.',num2str(A(2)));
    end
    clear A E;

    % Creating TRIG vector to mark onset of retained BOLD volumes.
    % TRIG will start from the hard-coded FirstTrig+(Number of volumes
    % skipped), and increase by frequency step (number of measures per
    % volume), and end before exceeding the length of ACQ
    TRIG(FirstTrig+(VolSkip*FreqStep):FreqStep:end) = 1;

%% write to table
    % make table of physio outputs ordered TRIG,ACQ,EXT,RESP,PULS
    T = table(TRIG,ACQ,EXT,RESP,PULS);
    % csv outname will match the UUID from the corresponding BOLD data
    outname=strcat('Physio_combined_',physio.UUID{1,1},'.csv');
    writetable(T,outname);  % Default delimiter is csv

catch ME
    % print dicom name and warning type if there is one in the body of
    % function
    fprintf('WARNING: %s in %s',ME.message,fn)
end
