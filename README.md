# README #

This repository contains the LifeSpan physio generation MATLABL scripts from Greg B.

* These scripts were originally located in /nrgpackages/tools.release/intradb/CMRRPhysio
* See https://bitbucket.org/hcp/physio-ev for the wrapper scripts used to call these.

